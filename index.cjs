const http = require('http');
const path = require('path');
const uuid = require('uuid');

const fs = require('fs');

const PORT = process.env.PORT || 8000;

const server = http.createServer((req, res) => {
    if (req.url === '/html') {
        const filePath = path.resolve("index.html");
        fs.readFile(filePath, (err, data) => {
            if (err) {
                console.error(err)
            } else {
                res.write(data);
                res.end();
            }
        })

    } else if (req.url === '/json') {
        const filePath = path.resolve("data.json");
        fs.readFile(filePath, (err, data) => {
            if (err) {
                console.error(err);
            } else {
                res.write(data);
                res.end();
            }
        })

    } else if (req.url === '/uuid') {
        const uniqueId = uuid.v4();
        res.write(`{"uuid": "${uniqueId}"}`);
        res.end();

    } else if (req.url.startsWith('/status')) {
        let code = req.url.split('/');
        code = code[code.length - 1];
        if (isNaN(code)) {
            res.statusCode = 422;
            res.write('Status Code must be a number');
            res.end();
        } else {

            statusCodes = require('http').STATUS_CODES;
            res.statusCode = code;
            res.write(statusCodes[code]);
            res.end();
        }

    } else if (req.url.startsWith('/delay')) {
        let time = req.url.split('/');
        time = parseInt(time[time.length - 1]);
        if (isNaN(time)) {
            res.statusCode = 422;
            res.write('Time must be a valid number');
            res.end();
        } else {
            setTimeout(() => {
                res.write(`Page Loaded after ${time} second`);
                res.statusCode = 200;
                res.end();
            }, time * 1000);
        }
    } else {
        res.statusCode = 400;
        res.write("Please Enter a valid url");
        res.end();
    }
});


server.listen(PORT, () => {
    console.log("Server listening on " + PORT);
});
